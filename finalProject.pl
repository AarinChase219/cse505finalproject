:- import load_xml_structure/3 from sgml.
:- import parse_xpath/4 from xpath.

changeXMLNode(InputFile):-
    load_xml_structure(file(InputFile), Term, Warn),
    write(Term).

queryLanguage(InputFile, XPath, ReturnValue):-
    parse_xpath(file(InputFile), XPath, ReturnValue, 'x=http://www.w3.org/1999/xhtml').

#findData([element(X, Y, Z)|T], , ReturnValue):-
#    X == 

findData([H|T], XPath, ReturnValue):-
    findData(T, XPath, ReturnValue).

findData([], XPath, []).

implementationQuery(InputFile, XPath, ReturnValue):-
    load_xml_structure(file(InputFile), Term, Warn),
    findData(Term, XPath, ReturnValue).
